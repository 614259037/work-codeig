<?php defined('BASEPATH') or exit('No direct script access allowed');
class user_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function register($username, $concealpass, $fname, $lname)
    {
        $this->db->where('u_username', $username);
        $result = $this->db->get('user');
        $state = "";
        if ($result->num_rows() <= 0) {
            $user_data = array(
                'u_username' => $username,
                'u_password' => $concealpass,
                'u_fname' => $fname,
                'u_lname' => $lname,
                'u_status' => "guest"
            );
            $query =  $this->db->insert('user', $user_data);
            if ($query) {
                $state =  "success";
            }
        } else {
            $state =  "false";
        }
        return $state;
    }
}
