<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

	function __construct()
	{
		header("Access-Control-Allow-Origin: *");
		header("Access-Control-Allow-Methods:GET, POST ,OPTIONS, PUT, DELETE");
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		parent::__construct();
		$this->load->model('user_model', 'user');
	}

	public function register()
	{
		$getdata = json_decode(file_get_contents('php://input'), true);
		$username = $getdata['username'];
		$fname = $getdata['fname'];
		$lname = $getdata['lname'];
		$password = $getdata['password'];
		$concealpass = md5($password);
		$result = $this->user->register($username, $concealpass, $fname, $lname);
		echo json_encode($result);
	}
}
